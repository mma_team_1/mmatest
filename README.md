## Requerimientos para la instalación

* Sistema opetativo (mac, linux, windows)
* [docker](https://www.docker.com/)
* [GIT](https://git-scm.com/downloads)

>No se necesita tener servidores web ni de base de datos, toda la arquitectura se virtualiza con docker, el detalle se encuentra en el archivo docker-compose.yml.

## Instalación del proyecto##


En el directorio `~/projects` se debe clonar el repositorio del proyecto

```

$ git clone git@bitbucket.org:mma_team_1/mmatest.git

```

Nos posicionamos en el proyecto.

```

$ cd mmatest

```

Instalamos las dependencias de laravel

```

$ docker run --rm -v $(pwd):/app composer install

```

Creamos nuestro archivo de entorno a partir del que laravel nos proporciona.

```

$ cp .env.example .env

```

Creamos nuestro archivo docker-compose.yml a partir del que se nos proporciona de ejemplo

```

$ cp docker-compose.yml.example docker-compose.yml

```

Modificamos el docker-compose.yml

En la línea 12 se debe modificar por la ruta absoluta en donde se encuentra el proyecto.

Ejemplo:

```

 # The Application
  app:
    build:
      context: ./
      dockerfile: app.dockerfile
    working_dir: /var/www
    volumes:
      - /Users/username/projects/mmatest:/var/www
    environment:
      - "DB_PORT=3306"
      - "DB_HOST=database"
se modifica por:

 # The Application
  app:
    build:
      context: ./
      dockerfile: app.dockerfile
    working_dir: /var/www
    volumes:
      - c:/users/username/projects/mmatest:/var/www
    environment:
      - "DB_PORT=3306"
      - "DB_HOST=database"

```

Guardamos e iniciamos los servidores web, aplicación y base de datos.


```

$ docker-compose up

```

Cuando finalice en otra consola (cmd+d) o (cmd+shift+d) entramos al contenedor de aplicación.


```

$ docker exec -it mmatest /bin/bash

```

Dentro del contenedor generamos la clave de la aplicación


```

$ php artisan key:generate

```

Optimizamos php artisan

```

$ php artisan optimize

```

Con esto tenemos laravel listo para comenzar

[http://localhost:8080/](http://localhost:8080)


Base de datos

Php Myadmin: [http://localhost:8888/](http://localhost:8888/)

Usuario : homestead Password : secret