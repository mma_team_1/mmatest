![api-laravel.png](https://bitbucket.org/repo/8zgEgyM/images/3317087063-api-laravel.png)

# Api Restful con framework laravel 

[TOC]

# Requisitos del sistema

* [git](https://git-scm.com/downloads)
* [docker ce](https://docs.docker.com/docker-for-mac/install/)

# Instalación del proyecto

En el directorio `~/projects` se debe clonar el repositorio del proyecto

```

$ git clone git@bitbucket.org:gignosoft/api_rest_full_laravel.git
```

Nos posicionamos en el proyecto.

```

$ cd api_rest_full_laravel
```

Instalamos las dependencias de laravel

```
$ docker run --rm -v $(pwd):/app composer install
```

Creamos nuestro archivo de entorno a partir del que laravel nos proporciona.

```

$ cp .env.example .env
```

Creamos nuestro archivo `docker-compose.yml` a partir del que se nos proporciona de ejemplo

```

$ cp docker-compose.yml.example docker-compose.yml
```

Modificamos el `docker-compose.yml`

En la línea 12 se debe modificar el nombre de usuario por el que corresponda al de su máquina.

Ejemplo:

```
 # The Application
  app:
    build:
      context: ./
      dockerfile: app.dockerfile
    working_dir: /var/www
    volumes:
      - /Users/username/projects/api_rest_full_laravel:/var/www
    environment:
      - "DB_PORT=3306"
      - "DB_HOST=database"
```

se modifica por:

```
 # The Application
  app:
    build:
      context: ./
      dockerfile: app.dockerfile
    working_dir: /var/www
    volumes:
      - /Users/lherrera/projects/api_rest_full_laravel:/var/www
    environment:
      - "DB_PORT=3306"
      - "DB_HOST=database"
```

Guardamos e iniciamos los servidores web, aplicación y base de datos.

```

$ docker-compose up
```

Cuando finalice en otra consola (cmd+d) o (cmd+shift+d) entramos al contenedor de aplicación.

```
$ docker exec -it apirestfulllaravel_app_1 /bin/bash
```

Dentro del contenedor generamos la clave de la aplicación

```

$ php artisan key:generate
```

Optimizamos php artisan

```

php artisan optimize
```

Con esto tenemos laravel listo para comenzar

[http://localhost:8080/](http://localhost:8080/)

![Image](https://bitbucket.org/repo/8zgEgyM/images/839943366-lara_01.png)

# Base de datos

Php Myadmin: [http://localhost:8888/](http://localhost:8888/)

Usuario     : homestead
Password    : secret